---
path: /blog/Diving-into-foss
date: "2023-09-20"
datestring: "20th September 2023"
cover: "./onboarding.jpeg"
tag: "My Onboarding experience with FOSSNSS"
author: "Rayhana27"
name: "Rayhana S"
title: "Diving into FOSS"
desc: "My onboarding experience to FOSSNSS"
---


![](./onboarding.jpeg)

## DAY 1
Firstly, I was a bit taken back when I heard about  FOSS NSSCE.As a beginner, I am new to coding so I thought the community was all about coding related activities. However, on the first day of community meeting our seniors gave a brief description regarding free and open source software(FOSS).They shared their experiences on being part of this community.It was clear that FOSS enthusiasts are passionate about the principles of openness, collaboration, and innovation.Introduced us what open source coding is all about and people with not much prior knowledge can also contribute something to the community through their the interest and willingness to explore. At first it was a bit difficult to catch up on the things and softwares that they were sharing with us.It's not like I understood everything. But I got to know many interesting and fun things in the technical world. We got to know how important git hub / git lab is. They encouraged us to create git hub account. Also insisted us to switch to linux environment. Our seniors also gave us an overview about google summer of code and hacktoberfest. I appreciate how the speakers explained the importance of FOSS in education, emphasizing cost-effectiveness and the ability to customize software for learning purposes. It made me realize that FOSS can significantly benefit students like me by providing access to powerful tools without the financial burden and discovered the diverse range of skills within the FOSS NSSCE community, realizing that it's not just about coding; there are opportunities for designers, testers, documenters, and more.It was a very fun evening.I felt like being accepted and part of the community right after our first meeting.

## DAY 2
On our second meeting we got to know the entire members of the FOSS NSSCE.They shared with us what CTF is and various other websites.In cyber security, capture the flag (CTF) is a popular competition and training exercise that attempts to thoroughly evaluate participants' skills and knowledge in various subdomains.The goal of each CTF challenge is to find a hidden file or piece of information (the “flag”) somewhere in the target environment. It was quite tricky to understand all this on our first try. But still it was not at all boring. We were deeply invested in exploring new websites and stuffs.Moreover, the sense of community and the willingness to help newcomers was inspiring. It encouraged me to consider contributing to FOSS projects, knowing that there's a supportive network to guide and mentor me.As a beginner I got introduced to various cool apps like BlackHole and various other websites.Got to experience more of the technical world.I am grateful that I got this opportunity to be a part of such a wonderful and a heart warming community.Overall, this meeting has expanded my understanding of FOSS, its impact on education, and the opportunities it presents for students like me. I'm excited to explore this world further and potentially contribute to the FOSS community in the future.
Looking forward to more of these adventures in the future.
THANK YOU!