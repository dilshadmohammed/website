---
path: /events/CodeSync 2.0 A Git Gathering
date: "2023-12-01"
datestring: "01 December 2023"
cover: "./codesync.png"
author: "noelty"
name: "Noel Tom Santhosh"
title: "CodeSync 2.0: A Git Gathering"
---

![Poster](./codesync.png)
Hola people 👋
Join us for an insightful workshop on GitHub organized by the FOSS Cell at NSSCE. Learn the essentials of version control with Git. To make the most of the workshop, make sure to have a GitLab account and access to a Linux machine. Don't miss this opportunity to enhance your skills in open-source collaboration!

**SPEAKERS:** Ambady Anand S, Kannan V M  
**VENUE:** CSE Programming lab  
🗓️ **DATE and TIME:** December 01 2023 09:00 AM - 04:00 PM  
