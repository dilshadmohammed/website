---
path: /events/GSoC Sprint Camp
date: "2023-11-07"
datestring: "7 November 2023"
cover: "./gsoc.jpeg"
author: "noelty"
name: "Noel Tom Santhosh"
title: "GSoC Sprint Camp"
---

![Poster](./gsoc.jpeg)
FOSSNSS is planning on conducting a camp for the students interested in  participating in the upcoming  Google Summer of Code(GSoC) 2023 which is an internship that promotes beginners to contribute to open-source projects . Those interested to contribute to open-source projects are welcome to attend.

**VENUE:** CSE Seminar Hall  
🗓️ **DATE and TIME:** November 7 2023 4:30 PM