---
path: /events/Beginner's Guide to Git
date: "2023-10-11"
datestring: "11 October 2023"
cover: "./git.jpg"
author: "noelty"
name: "Noel Tom Santhosh"
title: "Beginner's Guide to Git"
---

![Poster](./git.jpg)
Every year, Hacktoberfest is an occasion to promote contributions to open source software. FOSSNSS is holding talks during Hactoberfest'23 to promote learning about the open source community.Beginners will learn how to use git for their projects through this. supporting the git commands' use. Join us here!

**SPEAKER:** Niranjan V Ram  
**VENUE:** CSE Seminar Hall  
🗓️ **DATE and TIME:** October 11 2023 4:00 PM - 7:00 PM  
