---
path: /events/Liberate with Linux
date: "2023-10-10"
datestring: "10 October 2023"
cover: "./linux.jpg"
author: "noelty"
name: "Noel Tom Santhosh"
title: "Liberate with Linux"
---

![Poster](./linux.jpg)
An annual event called Hacktoberfest promotes contributions to open-source software. To encourage learning more about the world of open source, FOSSNSS is hosting an introduction to Linux session as part of Hactoberfest'23. During the session, newcomers to Linux will be introduced and made comfortable with the fundamental commands. Join us here!

**SPEAKER:** Suneesh S  
**VENUE:** CSE Seminar Hall  
🗓️ **DATE and TIME:** October 10 2023 4:00 PM - 7:00 PM